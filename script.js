const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
let dateStateFirstCalendar = new Date();
let dateStateSecondCalendar = new Date();

window.addEventListener('load', () => {
    renderDate();

    let buttons = document.querySelectorAll('input');
    for (let button of buttons) 
        button.addEventListener('click', onClickBtn);
    
    let spans = document.querySelectorAll('span');
    for (let span of spans)
        span.addEventListener('click', onClickBtn);
    
});

//#region Simple Calendar
// Creates a new html-layout for a new month from State
const createDaysBlocks = (numberOfDays, actualDate) => {
    document.querySelector('#days').innerHTML = '';
    for (let i = 0; i < numberOfDays; i++) {
        let div = document.createElement('div');
        div.innerHTML = i + 1;
        div.classList.add('day');
        let today = new Date();
        if (i + 1 == today.getDate() && dateStateFirstCalendar.getMonth() == today.getMonth() && dateStateFirstCalendar.getFullYear() == today.getFullYear()) div.classList.add('today');
        document.querySelector('#days').appendChild(div);
    }
}

const onClickBtn = (event) => {
    switch (event.currentTarget.id) {
        case "previos": calcDateStateAndRender(dateStateFirstCalendar, 0); break;
        case "next": calcDateStateAndRender(dateStateFirstCalendar, 2); break;
        case "prev-cln": calcDateStateAndRender(dateStateSecondCalendar, 0); break;
        case "next-cln": calcDateStateAndRender(dateStateSecondCalendar, 2); break;
    }
};

// Returns the number of days in specified month
const getNumberofDates = (actualDate) => {
    let newDate = new Date(actualDate.getUTCFullYear(), actualDate.getMonth() + 1, 0);
    return newDate.getDate();
}

// Render actual RenderState
const renderDate = () => {
    document.querySelector('#year-month').innerHTML = `${dateStateFirstCalendar.getUTCFullYear()} ${months[dateStateFirstCalendar.getMonth()]}`;
    document.querySelector('#year-month-second').innerHTML = `${dateStateSecondCalendar.getUTCFullYear()} ${months[dateStateSecondCalendar.getMonth()]}`;
    createDaysBlocks(getNumberofDates(dateStateFirstCalendar), dateStateFirstCalendar.getDate());
    createCalendar(getNumberofDates(dateStateSecondCalendar));
}

// Calculate date State on next or previos Month and renders it
const calcDateStateAndRender = (date, correctMonth) => {
    if (date == dateStateFirstCalendar)
        dateStateFirstCalendar = new Date(dateStateFirstCalendar.getUTCFullYear(), dateStateFirstCalendar.getMonth() + correctMonth, 0);
    if (date == dateStateSecondCalendar)
        dateStateSecondCalendar = new Date(dateStateSecondCalendar.getUTCFullYear(), dateStateSecondCalendar.getMonth() + correctMonth, 0);
    renderDate();
}
//#endregion


const createCalendar = (numberOfDays) => {
    clearTable();

    // Find first day of month and it weekday index (Sunday i put as 7-index)
    let firstDayOfMonth = new Date(dateStateSecondCalendar.getUTCFullYear(), dateStateSecondCalendar.getMonth(), dateStateSecondCalendar.getDate() +1 - dateStateSecondCalendar.getDate());

    let dayOfWeek = firstDayOfMonth.getDay();
    if (dayOfWeek == 0) dayOfWeek = 7; // Date.getDate() returns Sunday as 0. Really?? wtf

    let firstRow = true;
    let counter = 1;
    
    // Create rows in first cycle and columns in second. Find the first day of month and its weekday. And skip some days in first week if needed
    while (counter < numberOfDays + 1) {
        let tr = document.createElement('tr');

        for (let a = 0; a < 7; a++){
            let td = document.createElement('td');
            if (firstRow && a < dayOfWeek-1) {
                tr.appendChild(td);
                continue;
            }
            firstRow = false;
            td.innerHTML = counter++;

            // Check the date. If today - add a class
            let today = new Date();
            if (counter -1 == today.getDate() && dateStateSecondCalendar.getMonth() == today.getMonth() && dateStateSecondCalendar.getFullYear() == today.getFullYear()) td.classList.add('today');

            tr.appendChild(td);
            if (counter >= numberOfDays+1) break;
        }
        document.querySelector('table').appendChild(tr);
    }

}

// Clear table rows when toggle monthes
const clearTable = () => {
    let rows = document.querySelectorAll('tr');
    for (let i = 1; i < rows.length; i++) 
        rows[i].remove();
}











